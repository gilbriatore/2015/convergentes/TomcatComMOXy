package br.edu.up;

import javax.xml.bind.annotation.*;

public class Telefone {
 
    private String tipo;
    private String numero;
 
    @XmlAttribute
    public String getTipo() {
        return tipo;
    }
 
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
 
    @XmlValue
    public String getValue() {
        return numero;
    }
 
    public void setNumero(String numero) {
        this.numero = numero;
    }
}
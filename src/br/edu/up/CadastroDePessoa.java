package br.edu.up;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("pessoas")
public class CadastroDePessoa {

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Pessoa read() {
		Pessoa pessoa = criarPessoa(0);
		return pessoa;
	}

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Path("{id}")
	public Pessoa read(@PathParam("id") int id) {

		Pessoa pessoa = criarPessoa(id);
		return pessoa;
	}

	private Pessoa criarPessoa(int id) {
		Pessoa pessoa = new Pessoa();
		pessoa.setId(id);
		pessoa.setNome("Marcos da Silva");

		Telefone telefone = new Telefone();
		telefone.setTipo("Celular");
		telefone.setNumero("9999");
		pessoa.getTelefones().add(telefone);
		
		Telefone telefone2 = new Telefone();
		telefone2.setTipo("Residencial");
		telefone2.setNumero("8888");
		pessoa.getTelefones().add(telefone2);
		return pessoa;
	}
}
package br.edu.up;

import java.util.*;
import javax.xml.bind.annotation.*;
 
@XmlRootElement
public class Pessoa {
	
	//JAXB (JSR-222) metadata the XML representation
 
    private int id;
    private String nome;
    private List<Telefone> telefones = new ArrayList<Telefone>();
 
    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getNome() {
        return nome;
    }
 
    public void setNome(String nome) {
        this.nome = nome;
    }
 
    @XmlElementWrapper
    @XmlElement(name="telefone")
    public List<Telefone> getTelefones() {
        return telefones;
    }
}